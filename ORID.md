O(Objective):
+ A Code Review was conducted in the group. &nbsp;While reading the code written by others in the group, I learned about the invocation of the Stream API and the implementation of polymorphism, which helped me better understand these concepts and methods.
+ Today I learned TDD, which means Test-driven development. &nbsp;It is helpful for programming to first clarify the test examples and test boundaries when completing a certain function.
+ In the afternoon, I practiced the TDD development method. During the exercise, I gradually developed the habit of Test-driven development. &nbsp;I hope I can keep this habit in future programming and minimize the impact of bugs in the code.
+ Learned the paradigm of creating unit tests.

R(Reflective):  
&emsp;&emsp;Fruitful

I(Interpretive):  
&emsp;&emsp;I am not very proficient in some shortcut keys in IDEA, and it is a bit difficult to follow the teacher to create projects in class. &nbsp;I will continue to familiarize myself with and practice the shortcut keys in IDEA in the future. &nbsp;I think maintaining the development habit of TDD is very important for improving my programming skills in the future.

D(Decision):  
&emsp;&emsp;Continue to learn and understand the concept of TDD, and maintain this habit in future programming. &nbsp;At the same time, continue to familiarize yourself with using the shortcut keys of IDEA.
