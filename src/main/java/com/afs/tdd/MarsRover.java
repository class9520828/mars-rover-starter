package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {

        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        } else if (command.equals(Command.Left)) {
            left();
        } else {
            right();
        }
    }

    private void move() {
        if (this.location.getDirection().equals(Direction.North)) {
            location.setCoordinateY(location.getCoordinateY() + 1);
        } else if (this.location.getDirection().equals(Direction.South)) {
            location.setCoordinateY(location.getCoordinateY() - 1);
        } else if (this.location.getDirection().equals(Direction.East)) {
            location.setCoordinateX(location.getCoordinateX() + 1);
        } else {
            location.setCoordinateX(location.getCoordinateX() - 1);
        }
    }

    private void left() {
        if (this.location.getDirection().equals(Direction.North)) {
            location.setDirection(Direction.West);
        } else if (this.location.getDirection().equals(Direction.South)) {
            location.setDirection(Direction.East);
        } else if (this.location.getDirection().equals(Direction.East)) {
            location.setDirection(Direction.North);
        } else {
            location.setDirection(Direction.South);
        }
    }

    private void right() {
        if (this.location.getDirection().equals(Direction.North)) {
            location.setDirection(Direction.East);
        } else if (this.location.getDirection().equals(Direction.South)) {
            location.setDirection(Direction.West);
        } else if (this.location.getDirection().equals(Direction.East)) {
            location.setDirection(Direction.South);
        } else {
            location.setDirection(Direction.North);
        }

    }

    public Location getLocation() {
        return location;
    }

    public void executeBatchCommand(List<Command> batchCommand) {
        batchCommand.forEach(this::executeCommand);
    }
}
